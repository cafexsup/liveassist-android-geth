package com.cafex.gethinliddell.liveassistmobile;


import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.alicecallsbob.assist.sdk.core.Assist;

import java.io.InputStream;
import java.net.URL;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoanFormFragment extends Fragment{

    /** keep track of the watermark in case it changes **/
    private String currentWatermarkUrl = "";

    /** watermark cache key **/
    private final static String WATERMARK_CACHE_KEY = "com.cafex.gliddell.watermark";

    public LoanFormFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View inflatedView;
        Resources resources = getResources();

        switch (resources.getConfiguration().orientation)
        {
            case Configuration.ORIENTATION_LANDSCAPE:
                inflatedView = inflater.inflate(R.layout.fragment_loan_form_landscape, container, false);
                break;

            case Configuration.ORIENTATION_PORTRAIT:
                inflatedView = inflater.inflate(R.layout.fragment_loan_form_portrait, container, false);
                break;

            default:
                inflatedView = inflater.inflate(R.layout.fragment_loan_form_landscape, container, false);
                break;
        }

        //hide the relevant views from the agent
        View view = inflatedView.findViewById(R.id.lawsuit_group);
        view.setTag(Assist.PRIVATE_VIEW_TAG, true);
        view = inflatedView.findViewById(R.id.bankrupt_group);
        view.setTag(Assist.PRIVATE_VIEW_TAG, true);
        view = inflatedView.findViewById(R.id.affirmCheckBox);
        view.setTag(Assist.PRIVATE_VIEW_TAG, true);

        if (savedInstanceState != null) {

            currentWatermarkUrl = savedInstanceState.getString(WATERMARK_CACHE_KEY);
        }

        //inflate the layout for this fragment
        return inflatedView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(WATERMARK_CACHE_KEY, currentWatermarkUrl);

    }

    @Override
    public void onResume() {
        super.onResume();

        //what is the current shared preferences web url?
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(getActivity());
        final String watermarkUrl = preferences.getString(getString(R.string.watermark_url_key),
                getString(R.string.watermark_url_default));

        Bitmap bitmap = Watermark.getBitmap();

        //if the watermark URL is set and is different to the original watermark URL
        if ( !watermarkUrl.equals("") && !watermarkUrl.equals(currentWatermarkUrl) && bitmap != null) {
            ImageView watermarkImage = (ImageView) getView().findViewById(R.id.watermark);
            watermarkImage.setImageBitmap(bitmap);
        }
    }
}
