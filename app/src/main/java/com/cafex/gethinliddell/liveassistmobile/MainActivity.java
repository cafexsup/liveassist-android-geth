package com.cafex.gethinliddell.liveassistmobile;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.alicecallsbob.fcsdk.android.phone.VideoSurface;

public class MainActivity extends FragmentActivity implements AssistSession.AssistSessionListener {

    /** key for our instance state on whether we are showing the web or the loan application form */
    public static final String IS_SHOWING_WEB = "com.cafex.liveassist.isShowingWeb";

    /** Simple state engine to know if la is in progress or not **/
    private boolean laStarted = false;

    /** our Manager of the assist session **/
    private static AssistSession assistSession;

    /** Remote video resolution point **/
    private static final Point REMOTE_RESOLUTION = new Point(352, 240);

    /** Local video resolution point **/
    private static final Point LOCAL_RESOLUTION = new Point(120, 81);

    /*
     * Activity lifecycle methods
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //create view
        setContentView(R.layout.activity_main);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (savedInstanceState == null) {
            WebViewFragment webViewFragment = new WebViewFragment();
            fragmentManager.beginTransaction()
                    .add(R.id.contentContainer, webViewFragment)
                    .commit();
        }

        updateLogo();
    }

    private void updateLogo() {
        Bitmap watermark = Watermark.getBitmap();
        if (watermark != null) {
            ImageView logo = (ImageView) findViewById(R.id.logo);
            logo.setImageBitmap(watermark);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateLogo();

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //save our video view state
        View remoteVideoContainer = findViewById(R.id.remoteVideoContainer);
        outState.putBoolean(getString(R.string.videoViewIsVisible), remoteVideoContainer.isShown());

        //store whether we are currently running an LA session or not
        outState.putBoolean(getString(R.string.isLaStarted), laStarted);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        //restore whether we are currently running an LA session or not
        laStarted = savedInstanceState.getBoolean(getString(R.string.isLaStarted));

        if (assistSession != null) {
            assistSession.setListener(this);
            ViewGroup videoContainer = (ViewGroup) findViewById(R.id.remoteVideoContainer);
            Log.d("MainActivity", "Viewgroup Size = "+videoContainer.getChildCount());
            VideoSurface videoSurface = assistSession.getNewRemoteSurface();
            RelativeLayout.LayoutParams layoutParams= new RelativeLayout.LayoutParams(videoSurface.getLayoutParams());
            layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
            videoContainer.addView(videoSurface, 2, layoutParams);
            findViewById(R.id.videoProgressBar).setVisibility(View.GONE);
        }
    }

    /**
     * Open the settings screen
     */
    public void openSettings(View view)
    {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    /**
     * Load the home fragment.
     * @param view
     */
    public void home(View view)
    {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentContainer, new WebViewFragment())
                .addToBackStack(null)
                .commit();
    }

    /**
     * Load the data entry fragment
     * @param view
     */
    public void dataEntry(View view) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentContainer, new LoanFormFragment())
                .addToBackStack(null)
                .commit();
    }

    /**
     * Toggle LA on and off
     */
    public void toggleLa(View view)
    {
        //now start the process
        if (laStarted)
        {
            assistSession.stopAssist();
        } else
        {
            startAssist();
        }
    }

    private void startAssist()
    {
        //we have started the la process
        laStarted = true;

        //show video view with progress bar
        View progressbar = findViewById(R.id.videoProgressBar);
        progressbar.setVisibility(View.VISIBLE);

        //hide logo
        View logo = findViewById(R.id.logo);
        logo.setVisibility(View.INVISIBLE);


        //to start a video call, we first need to get a session token from the server
        //what is the current shared preferences la url?
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        final String laUrlString = preferences.getString(getString(R.string.la_server_url_key),
                getString(R.string.la_server_url_default));

        //we also need to know if we are running a native assist session, or whether we are letting
        //the web page run its own.
        final boolean nativeSession = preferences.getBoolean(getString(R.string.la_native_key), true);

        //now invoke the consumer service to get a session on a new thread
        assistSession = AssistSession.getNewAssistSession(laUrlString, this, getApplication(), LOCAL_RESOLUTION, REMOTE_RESOLUTION, nativeSession);
    }

    public void assistStopped() {
        //set our flag to stopped
        laStarted = false;

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //hide video view
                ViewGroup remoteVideoContainer = (ViewGroup) findViewById(R.id.remoteVideoContainer);
                View logo = findViewById(R.id.logo);
                logo.setVisibility(View.VISIBLE);
                remoteVideoContainer.removeViewAt(2);

                ViewGroup localVideoContainer = (ViewGroup) findViewById(R.id.localVideoContainer);
                localVideoContainer.removeViewAt(1);

                //remove our handle to the assist session
                assistSession = null;

                //TODO
                //webViewFragment.toggleAssist(null);

            }
        });
    }



    @Override
    public void assistCreationFailure(String failureString) {
    }

    @Override
    public void setVideoSurfaces(final VideoSurface localSurface, final VideoSurface remoteSurface) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ViewGroup remoteVideoContainer = (ViewGroup) findViewById(R.id.remoteVideoContainer);
                ViewGroup localVideoContainer = (ViewGroup) findViewById(R.id.localVideoContainer);

                remoteSurface.setVisibility(View.INVISIBLE);
                remoteVideoContainer.addView(remoteSurface, 2);

                RelativeLayout.LayoutParams layoutParams= new RelativeLayout.LayoutParams(0, 0);
                layoutParams.addRule(RelativeLayout.CENTER_IN_PARENT);
                layoutParams.addRule(RelativeLayout.INVISIBLE);
                localVideoContainer.addView(localSurface, 1, layoutParams);
                //localSurface.setVisibility(View.INVISIBLE);
            }
        });
    }

    @Override
    public void showRemoteSurface() {

        runOnUiThread(new Runnable() {
            public void run() {
                ViewGroup remoteVideoContainer = (ViewGroup) findViewById(R.id.remoteVideoContainer);
                remoteVideoContainer.getChildAt(2).setVisibility(View.VISIBLE);
                findViewById(R.id.videoProgressBar).setVisibility(View.GONE);
            }
        });

    }

    @Override
    public void showLocalSurface() {
        runOnUiThread(new Runnable() {
            public void run() {
                ViewGroup localVideoContainer = (ViewGroup) findViewById(R.id.localVideoContainer);
                localVideoContainer.getChildAt(1).setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void cobrowseStarted() {

    }

    @Override
    public void startWebViewCobrowse(final String cid) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                //TODO //webViewFragment.toggleAssist(cid);
            }
        });
    }
}
