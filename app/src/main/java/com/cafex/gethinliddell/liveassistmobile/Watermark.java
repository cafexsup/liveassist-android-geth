package com.cafex.gethinliddell.liveassistmobile;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.Toast;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by gliddell on 26/06/2015.
 */
public class Watermark {

    private static Bitmap bitmap;

    private static String currentUrl;

    public static void updateBitmap(final Context context, final String newBitmapUrl)
    {
        //if the newUrl is null, blank or has not changed then exit
        if (newBitmapUrl == null || newBitmapUrl.equals("") || newBitmapUrl.equals(currentUrl))
        {
            return;
        }

        currentUrl = newBitmapUrl;

        //network thread for getting image
        new Thread(new Runnable()
        {
            public void run()
            {
                try{
                    URL watermarkUrl = new URL(newBitmapUrl);
                    InputStream watermarkInputStream  = (InputStream) watermarkUrl.getContent();
                    bitmap = BitmapFactory.decodeStream((watermarkInputStream));

                    if (bitmap==null)
                        errorToast(context, "No bitmap at URL "+currentUrl);
                } catch (Exception e) {
                    errorToast(context, e.getMessage());
                }
            }

            private void errorToast(final Context context, final String error)
            {
                Handler handler = new Handler(context.getMainLooper());

                handler.post(new Runnable() {

                    public void run() {
                        Toast toast = Toast.makeText(context, "Failed to get watermark: " + error, Toast.LENGTH_SHORT);
                        toast.show();
                    }
                });
            }
        }).start();
    }

    public static Bitmap getBitmap()
    {
        return bitmap;
    }
}
