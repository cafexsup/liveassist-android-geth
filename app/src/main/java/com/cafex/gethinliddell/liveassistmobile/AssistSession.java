package com.cafex.gethinliddell.liveassistmobile;

import android.app.Application;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.preference.PreferenceManager;
import android.util.Base64;
import android.util.Log;

import com.alicecallsbob.assist.sdk.config.AssistConfig;
import com.alicecallsbob.assist.sdk.config.impl.AssistConfigBuilder;
import com.alicecallsbob.assist.sdk.core.Assist;
import com.alicecallsbob.assist.sdk.core.AssistError;
import com.alicecallsbob.assist.sdk.core.AssistListener;
import com.alicecallsbob.fcsdk.android.phone.Call;
import com.alicecallsbob.fcsdk.android.phone.CallCreationException;
import com.alicecallsbob.fcsdk.android.phone.CallListener;
import com.alicecallsbob.fcsdk.android.phone.CallStatus;
import com.alicecallsbob.fcsdk.android.phone.CallStatusInfo;
import com.alicecallsbob.fcsdk.android.phone.Phone;
import com.alicecallsbob.fcsdk.android.phone.VideoSurface;
import com.alicecallsbob.fcsdk.android.phone.VideoSurfaceListener;
import com.alicecallsbob.fcsdk.android.uc.UC;
import com.alicecallsbob.fcsdk.android.uc.UCFactory;
import com.alicecallsbob.fcsdk.android.uc.UCListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.X509TrustManager;

/**
 * Class for handling all network aspects of the Assist Session. This will create the session,
 * maintain the network connection, create the video call etc..
 *
 * All UI will be handled by the controlling activity.
 */
public class AssistSession implements UCListener, CallListener, VideoSurfaceListener, AssistListener, Serializable {
    /**
     * Define our logging context name *
     */
    public static final String LOG_TAG = AssistSession.class.getSimpleName();
    private String laUrlString;
    private UC uc;
    private Phone phone;
    private Call call;
    private transient VideoSurface remoteSurface;
    private transient VideoSurface localSurface;
    private AssistSessionListener listener;
    private final static int HTTP_RESPONSE_OK = 200;
    private Application application;
    private Point remoteVideoDimensions;
    private Point localVideoDimensions;
    private boolean nativeLa;

    /**
     * Factory method to get an instance of this class and to have it start its network
     * io on a thread
     * @param laUrlString The URL of the LA server to connect to
     */
    public static AssistSession getNewAssistSession(String laUrlString,
                                                    AssistSessionListener listener,
                                                    Application application,
                                                    Point localVideoDimensions,
                                                    Point remoteVideoDimensions,
                                                    boolean nativeLa)
    {
        final AssistSession sessionManager = new AssistSession(laUrlString, listener, application,
                localVideoDimensions, remoteVideoDimensions, nativeLa);
        new Thread(new Runnable() {
            @Override
            public void run() {
                sessionManager.createNewSession();
            }
        }).start();
        return sessionManager;
    }

    /**
     * Constructor only called from factory method.
     * @param laUrlString The URL of the LA server to connect to
     * @param listener A listener for the session to call back to on different events
     */
    private AssistSession(String laUrlString, AssistSessionListener listener,
                          Application application,
                          Point localVideoDimensions,
                          Point remoteVideoDiemensions,
                          boolean nativeLa) {
        this.laUrlString = laUrlString;
        this.listener = listener;
        this.application = application;
        this.remoteVideoDimensions = remoteVideoDiemensions;
        this.localVideoDimensions = localVideoDimensions;
        this.nativeLa = nativeLa;
    }

    /**
     * Creates new session state on the LA server to kick off the LA session.
     */
    private void createNewSession() {
        try {

            /*
            Use the HTTP session service inherent within the LA solution to create the session
            for the video calling
             */
            final URL serverUrl = new URL(laUrlString);
            final String base64Url = Base64.encodeToString(serverUrl.getHost().getBytes(), Base64.DEFAULT);
            final String urlEncodedQueryParams = URLEncoder.encode(base64Url, "UTF-8");

            final String getSessionUrl = laUrlString + application.getString(R.string.consumerUrlSuffix) + urlEncodedQueryParams;

            Log.d(LOG_TAG, getSessionUrl);

            URL url = new URL(getSessionUrl);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.connect();

            int response = conn.getResponseCode();
            InputStream is = conn.getInputStream();
            char[] buffer = new char[conn.getContentLength()];

            new InputStreamReader(is, "UTF-8").read(buffer);

            String content = new String(buffer);
            Log.d(LOG_TAG, content);

            /*
            check that we created the FCSDK session successfully.
             */
            if (response == HTTP_RESPONSE_OK) {

                JSONObject jsonObject = new JSONObject(content);
                String sessionToken = jsonObject.getString(application.getString(R.string.assist_session_token));
                String userId = jsonObject.getString(application.getString(R.string.assist_address));

                Log.d(LOG_TAG, "Session Token: " + sessionToken);
                Log.d(LOG_TAG, "UserID: " + userId);

                if (nativeLa)
                    startCobrowse(userId);
                else
                    listener.startWebViewCobrowse(userId);

                startVideo(sessionToken, userId);
            } else {
                Log.e(LOG_TAG, "Failed to get session ID: " + content);
                listener.assistCreationFailure(application.getString(R.string.session_creation_error));
            }
        } catch (IOException e) {
            Log.e(LOG_TAG, "Failed to create session", e);
            listener.assistCreationFailure(application.getString(R.string.session_creation_error));
        } catch (JSONException jsone) {
            Log.e(LOG_TAG, "Failed to parse JSON", jsone);
            listener.assistCreationFailure(application.getString(R.string.session_creation_error));
        } catch (Exception e) {
            Log.e(LOG_TAG, "Uncaught Exception", e);
            listener.assistCreationFailure(application.getString(R.string.session_creation_error));
        }
    }

    /**
     * Create the assist session and connect to it.
     * @param cobrowseId
     * @throws MalformedURLException
     */
    private void startCobrowse(String cobrowseId) throws MalformedURLException {
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(application);

        String la_server_url = preferences.getString(application.getString(R.string.la_server_url_key),
                application.getString(R.string.la_server_url_default));

        try {

            URL url = new URL(preferences.getString(application.getString(R.string.la_server_url_key), la_server_url));
            boolean secure = url.getProtocol().equals("https");
            int defaultPort = secure ? 443 : 80;
            AssistConfig config = new AssistConfigBuilder(application).
                    setServerHost(url.getHost()).
                    setServerPort(url.getPort() == -1 ? defaultPort : url.getPort()).
                    setCorrelationId(cobrowseId).
                    setAgentName("").
                    setConnectSecurely(secure).
                    setTrustManager(new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                        }

                        @Override
                        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {

                        }

                        @Override
                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                    }).
                    build();

            Assist.startSupport(config, application, this);

        } catch (MalformedURLException mue) {
            Log.e(LOG_TAG, "LA Server URL is not valid: " + la_server_url);
            throw mue;
        }
    }

    /**
     * Start the video call.
     * @param sessionToken
     * @param userId
     */
    private void startVideo(String sessionToken, String userId) {
        uc = UCFactory.createUc(application, sessionToken, this);

        //TODO need to properly implement network change listener
        uc.setNetworkReachable(true);
        uc.startSession();
    }

    /**
     * Stops the assist session killing both the video call and the assist session itself.
     */
    public void stopAssist()
    {
        //all tear down is triggered off the call finishing
        call.end();

        //nasty hack as we are not getting a call back when we end the call
        onStatusChanged(call, CallStatus.ENDED);
    }

    /**
     * UCListener methods *
     */
    @Override
    public void onSessionStarted() {

        phone = uc.getPhone();

        listener.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                startVideoCall();
            }
        });
    }

    private void startVideoCall() {
        //start the call
        try {
            SharedPreferences preferences = PreferenceManager
                    .getDefaultSharedPreferences(application);
            String la_agent_name = preferences.getString(application.getString(R.string.la_agent_key),
                    application.getString(R.string.la_agent_default));
            remoteSurface = phone.createVideoSurface(application, remoteVideoDimensions, this);
            localSurface = phone.createVideoSurface(application, localVideoDimensions, this);

            if (listener!=null) listener.setVideoSurfaces(localSurface, remoteSurface);
            listener.showLocalSurface();

            phone.setPreviewView(localSurface);

            call = phone.createCall(la_agent_name, true, true, this);
            call.setVideoView(remoteSurface);
        } catch (CallCreationException cce) {
            Log.e(LOG_TAG, "Call Creation Failed", cce);
        } catch (Exception e) {
            Log.e(LOG_TAG, "Uncaught Exception", e);
        }
    }

    VideoSurface getNewRemoteSurface()
    {
        remoteSurface = phone.createVideoSurface(application, remoteVideoDimensions, this);
        call.setVideoView(remoteSurface);
        return remoteSurface;
    }

    VideoSurface getNewLocalSurface()
    {
        localSurface = phone.createVideoSurface(application, remoteVideoDimensions, this);
        call.setVideoView(localSurface);
        return localSurface;
    }

    void setListener(AssistSessionListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onSessionNotStarted() {

    }

    @Override
    public void onSystemFailure() {

    }

    @Override
    public void onConnectionLost() {

    }

    @Override
    public void onConnectionRetry(int i, long l) {

    }

    @Override
    public void onConnectionReestablished() {

    }

    @Override
    public void onGenericError(String s, String s2) {

    }

    /**
     * VideoSurfaceListener Methods *
     */
    @Override
    public void onFrameSizeChanged(int i, int i2, VideoSurface.Endpoint endpoint, VideoSurface videoSurface) {
        Log.d(LOG_TAG, "onFrameSizeChanged");
    }

    @Override
    public void onSurfaceRenderingStarted(final VideoSurface videoSurface) {

        Log.d(LOG_TAG, "onSurfaceRenderingStarted");
    }

    /**
     * CallListener overrides *
     */

    @Override
    public void onDialFailed(Call call, String s, CallStatus callStatus) {

    }

    @Override
    public void onCallFailed(Call call, String s, CallStatus callStatus) {

    }

    @Override
    public void onMediaChangeRequested(Call call, boolean b, boolean b2) {

    }

    @Override
    public void onStatusChanged(Call call, CallStatus callStatus) {
        if (callStatus == CallStatus.ENDED)
        {
            if (nativeLa)
                Assist.endSupport();
            listener.assistStopped();
            listener = null;

            //causes a crash when ending the call from our side so removing for the moment
            //uc.stopSession();
        }
    }

    @Override
    public void onStatusChanged(Call call, CallStatusInfo callStatusInfo) {

    }

    @Override
    public void onRemoteDisplayNameChanged(Call call, String s) {

    }

    @Override
    public void onRemoteMediaStream(Call call) {
        listener.showRemoteSurface();
    }

    @Override
    public void onInboundQualityChanged(Call call, int i) {

    }

    @Override
    public void onRemoteHeld(Call call) {

    }

    @Override
    public void onRemoteUnheld(Call call) {

    }

    /*
     * AssistListener methods
     */
    @Override
    public void onSupportEnded(boolean b) {
        Log.d(LOG_TAG, "onSupportEnded: "+b);
    }

    @Override
    public void onSupportError(AssistError assistError, String s) {

    }

    /**
     * Created by gethinliddell on 09/04/2015.
     */
    public static interface AssistSessionListener {

        public void assistCreationFailure(String failureString);
        public void runOnUiThread(Runnable runnable);
        public void setVideoSurfaces(VideoSurface localSurface, VideoSurface remoteSurface);
        public void assistStopped();
        public void cobrowseStarted();
        public void startWebViewCobrowse(String cid);
        public void showRemoteSurface();
        public void showLocalSurface();

    }
}